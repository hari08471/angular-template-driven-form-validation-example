import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
export class Student {
  public name!: string;
  public email!: string;
  public password!: string;
  public subjects!: string;
}

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.css']
})
export class TemplateFormComponent implements OnInit {
  model = new Student();

  Subjects: string[] = [
    'Science',
    'Math',
    'Physics',
    'Finance'
  ];
  constructor(private router: Router) { }

  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  submit(data: any) {
    console.log(data.value)
  }

  

}
